package Fraction;

public class Fraction {

	final private int numerateur;
	final private int denominateur;
	
	public static final Fraction UN = new Fraction(1, 1);
	public static final Fraction ZERO = new Fraction(0, 1);
	
public Fraction(int numerateur, int denominateur) {
		
	if (denominateur == 0)  throw new ArithmeticException("le denominateur ne peut jamais �tre �gal � z�ro");
	if (denominateur < 0) {
           				numerateur = -numerateur;
           				denominateur=-denominateur;
   					  }
   
	this.denominateur=denominateur;	
	this.numerateur=numerateur;
   
	}

public Fraction() {
	
				this.numerateur=0;
				this.denominateur=1;
}



public Fraction(int numerateur) {
		
		this.numerateur=numerateur;
		this.denominateur=1;
		
	}

public int getNumerateur() {
	return numerateur;
}

public int getDenominateur() {
	return denominateur;
}
	

public void Consultation(){
	System.out.println("Numerateur est "+numerateur);
	System.out.println("Denominateur est "+denominateur);
}

public void ConsultationFraction(){
	System.out.println( "Fraction : "+(double)numerateur/denominateur);
	}

public Fraction Ajouter(final Fraction fraction){
	
	if (fraction.numerateur == 0) return this;
    if (this.numerateur == 0)  return fraction;
	int den= fraction.denominateur * this.denominateur;
	int num=fraction.numerateur*this.denominateur + this.numerateur*fraction.denominateur;
	return new Fraction(num, den);
	 
}

public boolean Comparer(final Fraction fraction) {
   
	if (this.equals(fraction) || (this.numerateur == fraction.numerateur && this.denominateur == fraction.denominateur) ) return true;
	  
	  int x = (this.numerateur) * fraction.denominateur;
	  int y= ( this.denominateur) * fraction.numerateur;
  
	  if (x == y)  return true;
	  else  return false;
    
}

public String toString() {

	final String str;
    if (this.denominateur == 1) str = Integer.toString(numerateur);
    else if (this.numerateur == 0) str = "Zero";
    else str = this.numerateur + " / " + this.denominateur;
    return str;
}

public String ComparerSelonOrdreNaturel(final Fraction fraction){
	
	String str ="";
	int x,y;
	
	if (this.equals(fraction) || (this.numerateur == fraction.numerateur && this.denominateur == fraction.denominateur) ) {
		str= "les deux fractions sont egaux";;
	}
	  
	x = (this.numerateur) * fraction.denominateur;
	y= (this.denominateur) * fraction.numerateur;
    
    if (x == y) str= "les deux fractions sont egaux";
    
    else if (x > y) str= fraction.toString()+" < "+this.toString();
    
    else  str= fraction.toString()+" > "+this.toString();

	return str;
	
	
}

	
	
	
}
